import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:spend_tracker/firebase/firebase_bloc.dart';
import 'package:spend_tracker/models/item.dart';
import 'package:spend_tracker/models/models.dart';
import 'package:spend_tracker/routes.dart';

class ItemPage extends StatefulWidget {
  ItemPage({@required this.isDeposit});
  final int isDeposit;
  @override
  _ItemPageState createState() => _ItemPageState();
}

class _ItemPageState extends State<ItemPage> with RouteAware {
  Map<String, dynamic> _data = Map<String, dynamic>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  DateTime _dateTime = DateTime.now();
  bool _hasChanges = false;
  bool _isSaving = false;

  @override
  void initState() {
    super.initState();
    _data["isDeposit"] = widget.isDeposit;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    super.dispose();
    routeObserver.unsubscribe(this);
  }

  @override
  void didPopNext() {}

  @override
  void didPushNext() {}

  @override
  Widget build(BuildContext context) {
    var bloc = Provider.of<FirebaseBloc>(context);
    return Scaffold(
        appBar: AppBar(
          title: const Text("Item"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.save),
              onPressed: () async {
                if (!_formKey.currentState.validate()) {
                  return;
                }
                setState(() {
                  _isSaving = true;
                });
                _formKey.currentState.save();
                var bloc = Provider.of<FirebaseBloc>(context);
                _data['date'] = DateFormat("MM/dd/yyyy").format(_dateTime);
                var item = Item.fromMap(_data);
                await bloc.createItem(item);
                Navigator.pop(context);
              },
            )
          ],
        ),
        body: _isSaving
            ? Center(child: CircularProgressIndicator())
            : Form(
                key: _formKey,
                onWillPop: () {
                  if (_hasChanges) {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Confirm"),
                            content: const Text("Discard Changes?"),
                            actions: <Widget>[
                              FlatButton(
                                child: const Text("Yes"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                                },
                              ),
                              FlatButton(
                                child: const Text("No"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              )
                            ],
                          );
                        });
                    return Future.value(false);
                  }
                  return Future.value(true);
                },
                onChanged: () => _hasChanges = true,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                          decoration:
                              InputDecoration(labelText: "Item Description"),
                          validator: (String value) {
                            if (value.isEmpty) return "Required";
                          },
                          onSaved: (String value) =>
                              _data["description"] = value),
                      TextFormField(
                          keyboardType:
                              TextInputType.numberWithOptions(decimal: true),
                          decoration: InputDecoration(labelText: "Amount"),
                          validator: (String value) {
                            if (value.isEmpty) return "Required";
                            if (double.tryParse(value) == null)
                              return "Invalid Number";
                          },
                          onSaved: (String value) =>
                              _data["amount"] = double.parse(value)),
                      Row(
                        children: <Widget>[
                          Checkbox(
                              value: _data["isDeposit"] == 1 ? true : false,
                              onChanged: (bool value) {
                                setState(() {
                                  _data["isDeposit"] = value == true ? 1 : 0;
                                });
                              }),
                          const Text("Deposit")
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          IconButton(
                              icon: Icon(Icons.date_range),
                              onPressed: () async {
                                var date = await showDatePicker(
                                    context: context,
                                    initialDate: _dateTime,
                                    firstDate: DateTime.now()
                                        .add(Duration(days: -365)),
                                    lastDate: DateTime.now()
                                        .add(Duration(days: 365)));
                                if (date == null) return;
                                setState(() {
                                  _dateTime = date;
                                });
                              }),
                          Text(DateFormat('MM/dd/yyyy').format(_dateTime))
                        ],
                      ),
                      _AccountsDropdown(
                          bloc: bloc,
                          urlId: _data["accountUrlId"],
                          onChanged: (String value) {
                            _hasChanges = true;
                            setState(() {
                              _data["accountUrlId"] = value;
                            });
                          }),
                      _ItemTypesDropdown(
                          bloc: bloc,
                          urlId: _data["typeUrlId"],
                          onChanged: (String value) {
                            _hasChanges = true;
                            setState(() {
                              _data["typeUrlId"] = value;
                            });
                          }),
                    ],
                  ),
                )));
  }
}

class _ItemTypesDropdown extends StatelessWidget {
  const _ItemTypesDropdown(
      {Key key,
      @required this.bloc,
      @required this.urlId,
      @required this.onChanged})
      : super(key: key);

  final FirebaseBloc bloc;
  final String urlId;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<ItemType>>(
        stream: bloc.itemTypes,
        builder: (_, AsyncSnapshot<List<ItemType>> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          }
          if (!snapshot.hasData) {
            return Center(child: const Text("No Records"));
          }
          return DropdownButtonFormField<String>(
            value: urlId,
            decoration: InputDecoration(labelText: "Type"),
            items: snapshot.data
                .map((type) => DropdownMenuItem<String>(
                    value: type.urlId, child: Text(type.name)))
                .toList(),
            validator: (String value) => value == null ? "Required" : null,
            onChanged: onChanged,
          );
        });
  }
}

class _AccountsDropdown extends StatelessWidget {
  const _AccountsDropdown(
      {Key key,
      @required this.bloc,
      @required this.urlId,
      @required this.onChanged})
      : super(key: key);

  final FirebaseBloc bloc;
  final String urlId;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Account>>(
        stream: bloc.accounts,
        builder: (_, AsyncSnapshot<List<Account>> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          }
          if (!snapshot.hasData) {
            return Center(child: const Text("No Records"));
          }
          return DropdownButtonFormField<String>(
            value: urlId,
            decoration: InputDecoration(labelText: "Account"),
            items: snapshot.data
                .map((account) => DropdownMenuItem<String>(
                    value: account.urlId, child: Text(account.name)))
                .toList(),
            validator: (String value) => value == null ? "Required" : null,
            onChanged: onChanged,
          );
        });
  }
}
