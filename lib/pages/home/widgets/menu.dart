import 'package:flutter/material.dart';

class Menu extends StatelessWidget {
  const Menu({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var color = Theme.of(context).primaryColor;

    return SizedBox(
      width: 150,
      child: Drawer(
          child: Column(
        children: <Widget>[
          Container(
              height: 100,
              alignment: Alignment.bottomCenter,
              child:
                  Text("MENU", style: TextStyle(fontSize: 20, color: color))),
          Divider(height: 20, color: Colors.black),
          _MenuItem(
              icon: Icons.account_balance,
              title: "Accounts",
              color: color,
              onTap: () => onNavigate(context, "/accounts")),
          Divider(height: 20, color: Colors.black),
          _MenuItem(
              icon: Icons.attach_money,
              title: "Budget Items",
              color: color,
              onTap: () => onNavigate(context, "/items")),
          Divider(height: 20, color: Colors.black),
          _MenuItem(
              icon: Icons.widgets,
              title: "Types",
              color: color,
              onTap: () => onNavigate(context, "/types")),
          Divider(height: 20, color: Colors.black),
          _MenuItem(
              icon: Icons.security,
              title: "Logout",
              color: color,
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushReplacementNamed("/");
              })
        ],
      )),
    );
  }

  void onNavigate(BuildContext context, String url) {
    Navigator.of(context).pop();
    Navigator.of(context).pushNamed(url);
  }
}

class _MenuItem extends StatelessWidget {
  const _MenuItem(
      {Key key,
      @required this.icon,
      @required this.title,
      @required this.color,
      @required this.onTap})
      : super(key: key);

  final IconData icon;
  final String title;
  final Color color;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Opacity(
        opacity: 0.6,
        child: Container(
            height: 70,
            alignment: Alignment.center,
            child: Column(
              children: <Widget>[
                Icon(
                  icon,
                  color: color,
                  size: 50,
                ),
                Text(title,
                    style: TextStyle(
                        color: color,
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500))
              ],
            )),
      ),
    );
  }
}
