import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:spend_tracker/firebase/firebase_bloc.dart';
import 'package:spend_tracker/models/item_type.dart';

import 'type_page.dart';

class TypesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var bloc = Provider.of<FirebaseBloc>(context);
    return Scaffold(
        appBar: AppBar(
          title: const Text("Types"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () => {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => TypePage()))
                  },
            )
          ],
        ),
        body: StreamBuilder<List<ItemType>>(
            stream: bloc.itemTypes,
            builder: (_, AsyncSnapshot<List<ItemType>> snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(snapshot.error.toString()),
                );
              }
              if (!snapshot.hasData) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              var itemTypes = snapshot.data;
              if (itemTypes.length == 0) {
                return Center(
                  child: const Text("No Records"),
                );
              }
              return ListView.builder(
                  itemCount: itemTypes.length,
                  itemBuilder: (_, int index) {
                    var itemType = itemTypes[index];
                    return ListTile(
                        leading: Hero(
                            tag: itemType.urlId,
                            child: Icon(itemType.iconData)),
                        title: Text(itemType.name),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) =>
                                      TypePage(itemType: itemType)));
                        });
                  });
            }));
  }
}
