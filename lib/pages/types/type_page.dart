import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spend_tracker/firebase/firebase_bloc.dart';
import 'package:spend_tracker/models/item_type.dart';
import 'package:spend_tracker/support/icon_helper.dart';
import 'package:spend_tracker/pages/icons/icon_holder.dart';

class TypePage extends StatefulWidget {
  TypePage({this.itemType});
  final ItemType itemType;
  @override
  _TypePageState createState() => _TypePageState();
}

class _TypePageState extends State<TypePage> {
  Map<String, dynamic> _data = Map<String, dynamic>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _hasChanges = false;
  bool _isSaving = false;

  @override
  void initState() {
    super.initState();
    if (widget.itemType != null) {
      _data = widget.itemType.toMap();
    } else {
      _data = Map<String, dynamic>();
      _data["codePoint"] = Icons.add.codePoint;
    }
  }

  @override
  Widget build(BuildContext context) {
    var bloc = Provider.of<FirebaseBloc>(context);
    return Scaffold(
        appBar: AppBar(
          title: const Text("Type"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                if (!_formKey.currentState.validate()) {
                  return;
                }
                setState(() {
                  _isSaving = true;
                });
                _formKey.currentState.save();
                var itemType = ItemType.fromMap(_data);
                if (itemType.urlId == null) {
                  bloc.createItemType(itemType);
                } else {
                  bloc.updateItemType(itemType);
                }
                Navigator.of(context).pop();
              },
            )
          ],
        ),
        body: _isSaving
            ? Center(child: CircularProgressIndicator())
            : Form(
                key: _formKey,
                onWillPop: () {
                  if (_hasChanges) {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Confirm"),
                            content: const Text("Discard Changes?"),
                            actions: <Widget>[
                              FlatButton(
                                child: const Text("Yes"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                                },
                              ),
                              FlatButton(
                                child: const Text("No"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              )
                            ],
                          );
                        });
                    return Future.value(false);
                  }
                  return Future.value(true);
                },
                onChanged: () => _hasChanges = true,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      IconHolder(
                        tagUrlId: widget.itemType == null
                            ? "0"
                            : widget.itemType.urlId,
                        newIcon: IconHelper.createIconData(_data["codePoint"]),
                        onIconChange: (IconData iconData) {
                          _hasChanges = true;
                          setState(() {
                            _data["codePoint"] = iconData.codePoint;
                          });
                        },
                      ),
                      TextFormField(
                          initialValue: widget.itemType != null
                              ? widget.itemType.name
                              : "",
                          decoration: InputDecoration(labelText: "Name"),
                          validator: (String value) {
                            if (value.isEmpty) return "Required";
                          },
                          onSaved: (String value) => _data["name"] = value),
                    ],
                  ),
                )));
  }
}
